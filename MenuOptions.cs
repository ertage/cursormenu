namespace CursorMenu;

public static class MenuOptions
{
    public static readonly List<string> FirstMenu = new()
    {
    "Start", //0
    "Stop", //1
    "Exit" //2
    };
    
    public static readonly List<string> SecondMenu = new()
    {
    "Add Option", //0
    "Exit" //1
    };
}